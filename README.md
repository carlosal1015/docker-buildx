# Docker BuildX 64 bit builder

Docker BuildX builder for use in Gitlab pipelines.

Example `.gitlab-ci.yml`:

```yaml
image: registry.gitlab.com/tiemen/docker-buildx:latest

stages:
  - build

variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  DOCKER_BUILDKIT: 1
  DOCKER_CLI_EXPERIMENTAL: enabled
  BUILDX_BUILDER: mybuilder # name of builder that is setup
  BUILDX_PLATFORM: linux/amd64,linux/aarch64 # platforms to support
  IMAGE: myimage
  TAG: latest

build-buildx:
  stage: build
  services:
    - name: docker:19.03.8-dind
      command: ["--experimental"]
  before_script:
    - . /bin/setup_builder.sh
    - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
  script:
    - docker buildx build --platform=${BUILDX_PLATFORM} -t ${CI_REGISTRY}/${IMAGE}:${TAG} .
    - docker push ${CI_REGISTRY}/${IMAGE}:${TAG}
```
