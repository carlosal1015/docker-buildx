#/bin/sh

docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --use --driver docker-container --name ${BUILDX_BUILDER} --platform=${BUILDX_PLATFORM}
docker buildx inspect --bootstrap ${BUILDX_BUILDER}
docker buildx ls
